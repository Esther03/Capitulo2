/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio16;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio16 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * (Comparación de enteros) Escriba una aplicación que pida al 
         * usuario que escriba dos enteros, que obtenga los números del 
         * usuario y muestre el número más grande, seguido de las palabras 
         * “es más grande”. Si los números son iguales, imprima el mensaje 
         * “Estos números son iguales”.
         */
        Scanner s=new Scanner(System.in);
        int n1, n2;
                     
        System.out.println("Dé el primer número entero: ");
        n1=s.nextInt();
        System.out.println("Dé el segundo número entero: ");
        n2=s.nextInt();
 
        if(n1>n2)
        System.out.printf("%d es más grande%n", n1);
        if(n1<n2)
        System.out.printf("%d es más grande%n", n2);
        if(n1 == n2)
        System.out.println("Estos números son iguales");

    }
    
}
