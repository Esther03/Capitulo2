/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio15;
import java.util.Scanner;

/**
 *
 * @author Cornejo Hernández María Esther
 */
public class Ejercicio15 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /**
         * Escriba una aplicación que pida al usuario que escriba dos números, 
         * que obtenga los números del usuario e imprima la suma, producto, 
         * diferencia y cociente (división) de los números.
         */
        Scanner s=new Scanner(System.in);
        int n1, n2;
        int suma, producto, diferencia, cociente;

        System.out.print("Escriba el primer número entero: ");
        n1=s.nextInt();
        System.out.print("Escriba el segundo número entero: ");
        n2=s.nextInt();
                      
        suma=n1+n2;
        producto=n1*n2;
        diferencia= n1-n2;
        cociente=n1/n2;
                      
        System.out.printf("La suma es: %d%n", suma);
        System.out.printf("El producto es: %d%n", producto);
        System.out.printf("La diferencia es: %d%n", diferencia);
        System.out.printf("La division es: %d%n", cociente);

    }
    
}
